package ru.lettuce.testapp.presentation.list

import ru.lettuce.testapp.R
import ru.lettuce.testapp.presentation.viewmodel.ExpandableViewModel
import ru.lettuce.testapp.presentation.viewmodel.ItemViewModel
import kotlin.reflect.KClass

/**
 * Class for mapping view model class and layout id
 *
 * @author Leonid Emelyanov
 */
class ViewTypes private constructor() {

    private val viewTypes = ArrayList<Pair<KClass<out Any>, Int>>()

    init {
        viewTypes.add(Pair(ExpandableViewModel::class, R.layout.exandable_view_item))
        viewTypes.add(Pair(ItemViewModel::class, R.layout.recycle_view_item))
    }

    fun getViewType(kClass: KClass<out Any>) = viewTypes.indexOfFirst { it.first == kClass }

    fun getLayoutId(viewType: Int) =
            if (viewType >= 0 && viewType < viewTypes.size) {
                viewTypes[viewType].second
            } else {
                android.R.layout.test_list_item
            }

    companion object {
        @JvmStatic
        val instance by lazy { ViewTypes() }
    }
}