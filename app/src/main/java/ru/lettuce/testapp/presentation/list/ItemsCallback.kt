package ru.lettuce.testapp.presentation.list

import android.support.v7.util.DiffUtil

class ItemsCallback : DiffUtil.ItemCallback<Any>() {

    override fun areItemsTheSame(oldItem: Any, newItem: Any) =
            oldItem === newItem

    override fun areContentsTheSame(oldItem: Any, newItem: Any) =
            oldItem == newItem
}