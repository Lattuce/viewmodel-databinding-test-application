package ru.lettuce.testapp.presentation.viewmodel

/**
 * Wrapper class for emmit events only ones with LiveData
 *
 * @author Emelyanov Leonid
 **/
class Event<out T>(private var content: T?) {

    fun getContent(): T? {
        val result = content
        content = null
        return result
    }
}