package ru.lettuce.testapp.presentation.list

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import ru.lettuce.testapp.BR

/**
 * Default view holder for [MainAdapter]
 *
 * @author Leonid Emelyanov
 */
class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding: ViewDataBinding? = DataBindingUtil.getBinding(itemView)

    fun bind(viewModel: Any) {
        binding?.setVariable(BR.vm, viewModel)
        binding?.executePendingBindings()
    }
}