package ru.lettuce.testapp.presentation.list

import android.arch.lifecycle.LifecycleOwner
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Default adapter for recycle view
 *
 * @author Leonid Emelyanov
 */
class MainAdapter(private val lifecycleOwner: LifecycleOwner,
                  private val viewTypes: ViewTypes) : ListAdapter<Any, MainViewHolder>(ItemsCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
            MainViewHolder(inflateView(parent, viewTypes.getLayoutId(viewType)))

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int) = viewTypes.getViewType(getItem(position)::class)

    private fun inflateView(parent: ViewGroup, layoutId: Int): View {
        DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(parent.context), layoutId, parent, false)
                ?.let {
                    it.setLifecycleOwner(lifecycleOwner)
                    return it.root
                }
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
    }
}