package ru.lettuce.testapp.presentation.bindingadapter

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import ru.lettuce.testapp.R
import ru.lettuce.testapp.presentation.list.MainAdapter
import ru.lettuce.testapp.presentation.list.ViewTypes

/**
 * Binding adapters for work with swipe refresh and list
 *
 * @author Leonid Emelyanov
 */

/**
 * Set refreshing state
 */
@BindingAdapter("isRefreshing")
fun setSwipeRefreshState(layout: SwipeRefreshLayout,
                         isRefresh: Boolean) {
    layout.isRefreshing = isRefresh
}

/**
 * Set refresh listener
 */
@BindingAdapter("refreshListener")
fun setSwipeRefreshListener(layout: SwipeRefreshLayout,
                            refreshListener: SwipeRefreshLayout.OnRefreshListener) {
    layout.setOnRefreshListener(refreshListener)
}

/**
 * Set clickable state for view
 */
@BindingAdapter("isClickable")
fun setClickableState(view: View,
                      isClickable: Boolean) {
    view.isClickable = isClickable
}

/**
 * Set view models list for [RecyclerView] with [MainAdapter]
 */
@BindingAdapter(value = ["items", "viewTypes"])
fun setItems(recycleView: RecyclerView,
             items: List<Any>,
             viewTypes: ViewTypes) {
    if (recycleView.adapter == null) {
        recycleView.adapter = MainAdapter(recycleView.context as LifecycleOwner, viewTypes)
    }

    (recycleView.adapter as MainAdapter).submitList(items)
}

/**
 * Show error dialog with specified message
 */
@BindingAdapter("errorMessage")
fun showErrorDialog(view: View,
                    message: MutableLiveData<String>) {
    message.value?.let {
        AlertDialog.Builder(view.context)
                .setTitle(R.string.dialog_error_title)
                .setMessage(it)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    message.value = null
                }
                .create()
                .show()
    }
}

/**
 * Rotate icon depend on boolean flag
 */
@BindingAdapter("expandIcon")
fun expandableIconState(imageView: ImageView,
                        expanded: Boolean) {
    if (expanded) {
        imageView.animate().rotation(180F)
    } else {
        imageView.animate().rotation(0F)
    }
}

/**
 * Change view visibility
 */
@BindingAdapter("visibility")
fun setViewVisibility(view: View,
                      isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}