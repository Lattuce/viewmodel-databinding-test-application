package ru.lettuce.testapp.presentation.view

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.lettuce.testapp.BR
import ru.lettuce.testapp.R
import ru.lettuce.testapp.databinding.ActivityMainBinding
import ru.lettuce.testapp.presentation.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).apply {
            setLifecycleOwner(this@MainActivity)
            setVariable(BR.vm, ViewModelProviders.of(this@MainActivity).get(MainViewModel::class.java))
        }
    }
}