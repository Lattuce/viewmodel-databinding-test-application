package ru.lettuce.testapp.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData

/**
 * View model with list of children view models
 *
 * @author Emelyanov Leonid
 **/
class ExpandableViewModel(val title: String = "") {

    private val viewModels = ArrayList<Any>()

    val expanded = MutableLiveData<Boolean>().apply { value = false }
//    val onClick = MutableLiveData<Event<ExpandableViewModel>>()

    fun onClick() {
        expanded.postValue(expanded.value == false)
//        onClick.postValue(Event(this))
    }

    fun setItems(items: List<Any>) {
        viewModels.clear()
        viewModels.addAll(items)
    }

    fun getItems(): List<Any> {
        val result = ArrayList<Any>().apply { add(this@ExpandableViewModel) }

        if (expanded.value == true) {
            viewModels.forEach {
                if (it is ExpandableViewModel) {
                    result.addAll(it.getItems())
                } else {
                    result.add(it)
                }
            }
        }
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ExpandableViewModel) return false

        if (title != other.title) return false
        if (viewModels != other.viewModels) return false
        if (expanded != other.expanded) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + viewModels.hashCode()
        result = 31 * result + expanded.hashCode()
        return result
    }
}