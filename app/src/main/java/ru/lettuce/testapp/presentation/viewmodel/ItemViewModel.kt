package ru.lettuce.testapp.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.os.Handler

/**
 * View model for recycle view item
 *
 * @author Leonid Emelyanov
 */
open class ItemViewModel(val title: String = "") {

    private val handler = Handler()

    val isClicked = MutableLiveData<Boolean>().apply { value = false }

    open fun onClick() {
        handler.postDelayed({ isClicked.postValue(false) }, 2000)
        isClicked.postValue(isClicked.value == false)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ItemViewModel) return false

        if (title != other.title) return false
        if (handler != other.handler) return false
        if (isClicked != other.isClicked) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + handler.hashCode()
        result = 31 * result + isClicked.hashCode()
        return result
    }
}