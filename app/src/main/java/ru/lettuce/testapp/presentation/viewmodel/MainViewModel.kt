package ru.lettuce.testapp.presentation.viewmodel

import android.arch.lifecycle.*

/**
 * Main view model
 *
 * @author Leonid Emelyanov
 */
class MainViewModel : ViewModel(), LifecycleOwner {

    private val lifecycle = LifecycleRegistry(this).apply { markState(Lifecycle.State.RESUMED) }
    private val viewModels = ArrayList<ExpandableViewModel>()

    val isLoading = MutableLiveData<Boolean>().apply { value = false }
    val items = MutableLiveData<List<Any>>().apply { value = emptyList() }
    val error = MutableLiveData<String>()

    init {
        fillItems()
    }

    override fun getLifecycle() = lifecycle

    override fun onCleared() {
        lifecycle.markState(Lifecycle.State.DESTROYED)
    }

    fun onRefresh() {
        isLoading.postValue(true)
    }

    fun onCLick() {
        isLoading.postValue(isLoading.value == false)
    }

    fun onErrorClick() {
        error.postValue("Error message")
    }

    private fun fillItems() {
        viewModels.clear()
        viewModels.addAll(arrayListOf(
                getItem("1").apply {
                    setItems(arrayListOf(
                            getItem("1.1").apply {
                                setItems(arrayListOf(
                                        ItemViewModel("1.1.1"),
                                        ItemViewModel("1.1.2")
                                ))
                            },
                            getItem("1.2").apply {
                                setItems(arrayListOf(
                                        ItemViewModel("1.2.1")
                                ))
                            }
                    ))
                },
                getItem("2").apply {
                    setItems(arrayListOf(
                            getItem("2.2").apply {
                                setItems(arrayListOf(
                                        ItemViewModel("2.2.1")
                                ))
                            }
                    ))
                },
                getItem("3").apply {
                    setItems(arrayListOf(
                            ItemViewModel("3.1"),
                            ItemViewModel("3.2")
                    ))
                }
        ))
        items.postValue(viewModels)
    }

    private fun getItem(title: String = "") = ExpandableViewModel(title).apply {
        expanded.observe(this@MainViewModel, Observer {
            items.postValue(viewModels.flatMap { it.getItems() })
        })
    }
}