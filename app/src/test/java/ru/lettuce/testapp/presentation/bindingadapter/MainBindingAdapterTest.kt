package ru.lettuce.testapp.presentation.bindingadapter

import android.view.View
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

/**
 * @author Leonid Emelyanov
 */
class MainBindingAdapterTest {

    @MockK
    private lateinit var view: View

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun setViewVisibilityVisible() {
        setViewVisibility(view, true)

        verify { view.visibility = View.VISIBLE }
    }

    @Test
    fun setViewVisibilityGone() {
        setViewVisibility(view, false)

        verify { view.visibility = View.GONE }
    }
}